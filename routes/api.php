<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("/nominees", "NomineeController@showNominees");

Route::post("/nominees", "NomineeController@addNew");

Route::post("/notify", "NomineeController@sendNotification");

Route::get("/merge-photo", "NomineeController@mergePhoto");

Route::post("/find-name/", "NomineeController@findName");

Route::post("/upload-image", "NomineeController@uploadImage");
