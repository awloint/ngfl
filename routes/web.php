<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('closed');
});

Route::any('/make-poster/{image_id?}', 'NomineeController@makePoster');

Route::any('/share-poster/{image_id}/share.html', 'NomineeController@sharePage');

Route::get('/stats/nominees', 'StatsController@showNominees');

Route::get('/closed', function(){
    return view('closed');
});
