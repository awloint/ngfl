<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nominee extends Model
{
   protected $guarded = ["id"];
   protected $appends = ["nomination_count"];
   
   public function nominator() {
      return $this->hasMany(Nominator::class, "nominee_id");
   }
   
   private function fullName() {
      return trim(implode(" ", [
         $this->first_name,
         $this->last_name,
         $this->middle_name
      ]));
   }
   
   function setNameAttribute($name) {
      $parts = self::getNameParts($name, true);
      $this->attributes = array_merge($this->attributes, $parts);
   }
   
   function getFullNameAttribute() {
      return $this->fullName();
   }
   
   function nominationCount() {
      return $this->nominator()->count();
   }
   
   function getNominationCountAttribute() {
      return $this->nominationCount();
   }
   
   public static function add($details, $init = false) {
      $nominee = new Nominee($details);
      
      if (!$init) {
         $nominee->save();
      }
      
      return $nominee;
   }
   
   private static function getAllCombos($array) {
      $combinations = [];
      $words = sizeof($array);
      $combos = 1;
      
      for ($i = $words; $i > 0; $i--) {
         $combos *= $i;
      }
      
      while (sizeof($combinations) < $combos) {
         shuffle($array);
         $combo = $array;
         
         if (!in_array($combo, $combinations)) {
            $combinations[] = $combo;
         }
      }
      
      return $combinations;
   }
   
   public static $debugging = false;
   
   public static function lookFor($name) {
      $np = self::getNameParts($name, 1);
      
      if (isset($np["middle_name"]) && !$np["middle_name"]) {
         unset($np["middle_name"]);
      }
      
      $query = Nominee::query();
      
      if (!preg_match("/\s/", $name)) {
         $query = $query->orWhere(function($query) use ($name) {
            return $query->where("first_name", "LIKE", "%{$name}%")
               ->orWhere("middle_name", "LIKE", "{$name}%")
               ->orWhere("last_name", "LIKE", "{$name}%");
         });
      }
      else {
         $combos = self::getAllCombos($np);
         $order_groups = [];
         foreach ($combos as $name) {
            $ordered = [
               "first_name" => $name[0],
               "last_name" => $name[1]
            ];
      
            if (isset($name[2]) && $name[2]) {
               $ordered["middle_name"] = $name[2];
            }
      
            $order_groups[] = $ordered;
      
            $query = $query->orWhere(function ($query) use ($ordered) {
               return $query->where("first_name", "LIKE", "{$ordered['first_name']}%")
                  ->where("last_name", "LIKE", "{$ordered["last_name"]}%");
            });
      
            if (isset($ordered["middle_name"])) {
               $query = $query->where("middle_name", "LIKE", "{$ordered["middle_name"]}%");
            }
         }
      }
      
//      dd($query->toSql(), $query->get());
      
      if (self::$debugging) {
         dd("searching =>", $query->get(), $query->toSql(), $order_groups);
      }
      
      return $query->get();
   }
   
   public static function makeFromName($name) {
      $name_parts = self::getNameParts($name, 1);
      
      return self::add($name_parts, 1);
   }
   
   private static function getNameParts($name, $array = false) {
      $parts = explode(" ", $name);
      $sorted = [];
      
      foreach ($parts as $k => $p) {
         if (trim($p) != "") {
            $sorted[] = $p;
         }
      }
      
      $name_parts = [
         "first_name" => isset($sorted[0]) ? $sorted[0] : "",
         "last_name" => isset($sorted[1]) ? $sorted[1] : "",
         "middle_name" => isset($sorted[2]) ? $sorted[2] : ""
      ];
      
      return $array ? $name_parts : (object)$name_parts;
   }
   
   public static function withInfo($name, $city) {
      $nominee = self::lookFor($name)->first() ?: self::add([
         "name" => $name,
         "city" => $city
      ]);
      
      return $nominee;
   }
}
