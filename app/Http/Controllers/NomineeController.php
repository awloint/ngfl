<?php

namespace App\Http\Controllers;

use App\Http\Requests\NominateRequest;
use App\Nominator;
use Illuminate\Http\Request;

use App\Nominee;
use Mockery\CountValidator\Exception;

class NomineeController extends Controller
{
   function showNominees() {
      return response(Nominee::all());
   }
   
   function addNew(NominateRequest $request) {
      if ($request->validated()) {
         $data = $request->input();
         $nominee = Nominee::withInfo($data["nominee"], $data["city"]);
         
         if (!$nominee->exists()) {
            $nominee = Nominee::add([
               "name" => $data["nominee"],
               "city" => $data["city"]
            ]);
         }
         
         unset($data["nominee"]);
         unset($data["city"]);
         
         try {
            $nominator = Nominator::add($data);
            $nominator->choose($nominee);
         } catch (Exception $ex) {
            return response(["msg" => $ex->getMessage()], 403);
         }
      }
      
      return response(["msg" => "Your nomination was saved successfully"]);
   }
   
   function sendNotification(Request $request) {
      $info = $request->post('nominator');
      $message = "Hello, #first_name, thank you for nominating a next generation female leader for the NGFL roundtable. Are you interested in being a part of the of the NGFL roundtable? More information coming soon!";
      
      foreach ($info as $n => $v) {
         $message = str_replace("#{$n}", $v, $message);
      }
      
      $recipient = \App\InfobipSMS::formatPhone($info["phone"]);
      
      $sent = \App\InfobipSMS::sendSMS($recipient, $message);
      
      return response($sent);
   }
   
   function mergePhoto() {
      $jpg_file = resource_path("assets/images/picture.jpg");
      $photo_link = Nominator::makePoster($jpg_file);
      
      return response([
         "link" => $photo_link
      ]);
   }
   
   function sharePage($poster_id) {
      return view("sharePage", [
         "url" => url("/share-poster/{$poster_id}/share.html"),
         "image_url" => url("/merged/{$poster_id}.png"),
         "facebook_app_id" => env('FACEBOOK_APP_ID'),
         "share_redirect_page"=>env('FACEBOOK_SHARE_REDIRECT_URL')
      ]);
   }
   
   function findName(Request $req) {
      $name = $req->post("name");
      
      //Validate name..
      if (strlen($name) < 3) {
         return [];
      }
      
      //Find the name..
      $result = Nominee::lookFor($name)->map(function ($nominee) {
         return $nominee->full_name;
      });
      
      return response([
         "result" => $result,
         "length" => $result->count()
      ]);
   }
   
   function makePoster(Request $request, $image_id = "") {
      if( !$image_id ) {
         return view("choose-template");
      }
      
      $tpl_name = "cover_" . str_replace("tpl-", "", $image_id);
      
      return view("make-poster", [
         "poster_link"=>url("/posters/{$tpl_name}.png"),
         "image_id" => $image_id,
         "share_url" => url("/share-poster/{$image_id}/share.html"),
         "image_url" => url("/merged/{$image_id}.jpg"),
         "facebook_app_id" => env('FACEBOOK_APP_ID')
      ]);
   }
   
   function uploadImage(Request $request) {
      $img = $request->post('image_string');
      $img = str_replace('data:image/png;base64,', '', $img);
      $img = str_replace(' ', '+', $img);
      $data = base64_decode($img);
      $file_id = md5(microtime() . $request->post("name"));
      $file_name = strtolower($request->post("name")) . "_" . $file_id;
      $file = public_path("merged/{$file_name}.png");
      $success = file_put_contents($file, $data);
      
      if( $success ) {
         $share_url = url("/share-poster/{$file_name}/share.html");
   
         return response([
            "image_url"=>url("merged/{$file_name}.png"),
            "share_url"=>$share_url
         ]);
      } else {
         return response([
            "message"=>"Could not save your poster"
         ], 403);
      }
   }
}
