<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NominateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
          'first_name' => 'required|max:20',
          'last_name' => 'required|max:20',
          'middle_name' => 'nullable|max:20',
          'phone' => 'required|max:20',
          'email' => 'required|email|max:50',
          'city' => 'nullable|max:30',
          'reason' => 'required|max:200',
          'nominee' => 'required|max:50'
       ];
    }
}
