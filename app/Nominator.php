<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Mockery\CountValidator\Exception;

class Nominator extends Model
{
   protected $guarded = ["id"];
   
   public static function makePoster($jpg_file) {
      $png_file = resource_path("assets/images/cover_1.png");
      $png = imagecreatefrompng($png_file);
      $jpeg = imagecreatefromjpeg($jpg_file);
   
      list($width, $height) = getimagesize($jpg_file);
      list($newwidth, $newheight) = getimagesize($png_file);
   
      $new_height = ($width / $newwidth) * $height;
   
      $out = imagecreatetruecolor($newwidth, $newheight);
      imagecopyresampled($out, $jpeg, 0, 0, 0, 0, $newwidth, $new_height, $width, $height);
      imagecopyresampled($out, $png, 0, 0, 0, 0, $newwidth, $newheight, $newwidth, $newheight);
   
      $rand_name = "ngfl__" . rand(10,1999);
      $file_name = "merged/{$rand_name}.jpg";
      
      imagejpeg($out, $file_name, 100);
   
      $size = filesize($file_name);
      $output_name = "ngfl_{$width}{$height}{$size}.jpg";
      
      file_put_contents("merged/{$output_name}", file_get_contents($file_name));
      unlink($file_name);
      
      return "ngfl_{$width}{$height}{$size}";
   }
   
   public function nominee() {
      return $this->hasOne(Nominee::class, "id", "nominee_id");
   }

   public static function add($data = []) {
      try {
         $nominator = new Nominator($data);
         $nominator->save();
         return $nominator;
      } catch (QueryException $ex) {
         throw new Exception("You are only allowed to nominate once");
      }
   }

   public function nominate($nominee) {
      if( !$nominee instanceof Nominee ) {
         $nominee = Nominee::withName($nominee);
      }

      $chosen = $this->choose($nominee);

      $this->load("nominee");

      return $chosen;
   }

   public function choose(Nominee $nominee) {
      //Save the nominee if not existing..

      if( !isset($nominee->id) ) {
         $nominee->save();
      }

      //Check update nominee id
      $this->nominee_id = $nominee->id;
      $this->save();
   }
}
