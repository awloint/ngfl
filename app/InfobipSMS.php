<?php

namespace App;

use infobip\api\client\SendSingleTextualSms;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\sms\mt\send\textual\SMSTextualRequest;

class InfobipSMS
{
   private $config, $from;
   
   function __construct() {
      $username = env('INFOBIP_USERNAME');
      $password = env('INFOBIP_PASSWORD');
      $this->from = env('INFOBIP_SENDER_ID', "AWLO Intl");
      $this->config = new BasicAuthConfiguration($username, $password);
   }
   
   public function sendSingle($to, $message) {
      $client = new SendSingleTextualSms($this->config);
   
      $requestBody = new SMSTextualRequest();
      $requestBody->setFrom($this->from);
      $requestBody->setTo($to);
      $requestBody->setText($message);
   
      $response = $client->execute($requestBody);
      
      return $response;
   }
   
   public static function sendSMS($to, $message) {
      $sender = new InfobipSMS();
      return $sender->sendSingle($to, $message);
   }
   
   public static function formatPhone($phone_number) {
      if( strlen(floor($phone_number)) == 10 ) {
         $formatted = "234" . trim(floor($phone_number));
         return $formatted;
      } else {
         return $phone_number;
      }
   }
}
