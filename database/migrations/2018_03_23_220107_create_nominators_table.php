<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNominatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nominators', function (Blueprint $table) {
            $table->increments('id');
            $table->string("first_name", 45);
            $table->string("middle_name", 45)->nullable()->default("");
            $table->string("last_name", 45);
            $table->string("phone", 45);
            $table->string("email", 45);
            $table->string("gender", 45);
            $table->longText("reason");
            $table->unsignedInteger("nominee_id")->nullable();
            $table->unique(["first_name", "last_name"], "unique_name");
            $table->unique("email", "unique_email");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nominators');
    }
}
