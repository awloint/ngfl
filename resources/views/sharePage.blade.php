<!DOCTYPE html>
<html lang="en">
<head>
  <meta property="og:url" content="{{$url}}"/>
  <meta property="og:type" content="image"/>
  <meta property="og:title" content="NGFL"/>
  <meta property="og:description" content="NGFL"/>
  <meta property="og:image" content="{{$image_url}}"/>
  <meta property="fb:app_id" content="{{$facebook_app_id}}"/>
  <script>
    window.location = '{{$share_redirect_page}}';
  </script>
</head>

<body>
<div>
  <h1>NGFL</h1>
  <p>Nominate..</p>
  <img src="{{$image_url}}" alt="nominate-img" style="height: 400px"/>
</div>
</body>
</html>
