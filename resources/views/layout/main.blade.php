<!DOCTYPE html>
<html lang="en">
<head>
  <title>NGFL Nominations</title>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
        rel="stylesheet" id="bootstrap-css" />
  <link href="{{ url("/css/croppie.css") }}" rel="stylesheet"/>
  <link href="{{ url("/css/font-awesome.css") }}" rel="stylesheet"/>
  <link href="{{ url("/css/waiting.css") }}" rel="stylesheet"/>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
  <script src="{{url("/js/jquery.waiting.js")}}"></script>
  <style>
    .image-preview-input {
      position: relative;
      overflow: hidden;
      margin: 0px;
      color: #333;
      background-color: #fff;
      border-color: #ccc;
    }
    
    .image-preview-input input[type=file] {
      position: absolute;
      top: 0;
      right: 0;
      margin: 0;
      padding: 0;
      font-size: 20px;
      cursor: pointer;
      opacity: 0;
      filter: alpha(opacity=0);
    }
    
    .image-preview-input-title {
      margin-left: 2px;
    }
    </style>
  
  @yield('css')
</head>
<body>
<div class="container" id="root">
  <br/>
  @yield('content')
</div>

<!-- /container -->
@yield('scripts')
</body>

</html>
