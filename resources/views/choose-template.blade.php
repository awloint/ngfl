@extends('layout.main')

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading"><strong>Select DP Template</strong>
          <small></small>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                @for($x=1; $x<=3; $x++)
                  <div class="col-md-4">
                    <div class="image-area" ref="image_area_<?php echo $x; ?>">
                      <img class="img-rounded img-thumbnail"
                           src="{{url("/posters/cover_{$x}.png")}}"/>
                      <div class="name-label">
                        <span v-text="first_name" class="fnf"></span> <br/>
                        <span v-text="last_name" class="lnf"></span>
                      </div>
                    </div>
                    <div class="text-center">
                      <br />
                      <a href="{{url("/make-poster/tpl-{$x}")}}" class="btn btn-success">
                        <i class="fa fa-save"></i> Choose DP
                      </a>
                    </div>
                  </div>
                @endfor
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
