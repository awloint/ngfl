@extends('layout.main')


@section('css')
  <style rel="stylesheet" href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
  <style>
    /** Poster Style **/
    @font-face {
      font-family: 'hp_simplifiedregular';
      src: url('/fonts/ngfl/hp-simplified-webfont.woff2') format('woff2'),
      url('/fonts/ngfl/hp-simplified-webfont.woff') format('woff');
      font-weight: normal;
      font-style: normal;
      
    }
    
    @font-face {
      font-family: 'adam_cg_pro';
      src: url('/fonts/ngfl/adam_cg_pro.woff2') format('woff2'),
      url('/fonts/ngfl/adam_cg_pro.woff') format('woff');
      font-weight: normal;
      font-style: normal;
      
    }
    
    @font-face {
      font-family: 'october_twilightregular';
      src: url('/fonts/ngfl/october_twilight-webfont.woff2') format('woff2'),
      url('/fonts/ngfl/october_twilight-webfont.woff') format('woff');
      font-weight: normal;
      font-style: normal;
      
    }
    
    div.name-label {
      position: absolute;
      text-align: center;
      font-size: 29px;
      line-height: 1.3;
      z-index: 14;
      bottom: 0;
      left: 0;
      width: 243px;
      color: #000;
    }
    
    .fnf {
      font-family: "october_twilightregular";
    }
    
    .lnf {
      font-family: "adam_cg_pro";
    }
    
    .image-area {
      position: absolute;
      top: 25px;
      left: 25px;
      width: 92%;
    }
    
    .poster-cover {
      background-color: transparent;
      position: absolute;
      top: 0;
    }
    
    .poster-uploaded {
      width: 100%;
    }
  </style>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong>Upload your photo to make poster</strong>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-6">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>First Name</label>
                    <input type="text" v-model="first_name" class="form-control"/>
                  </div>
                  <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" v-model="last_name" class="form-control"/>
                  </div>
                  <div class="form-group" v-show="selected && !cropped">
                    <div class="col-md-6 col-md-offset-3 text-center">
                      <!-- or even simpler -->
                      <img ref="croppie-item" class="img img-fluid" :src="upload.image_link"
                           style="max-width: 200px;"/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Select Your Picture</label>
                    <div class="row">
                      <div class="col-sm-8">
                        <div class="input-group image-preview">
                          <input :value="file.name" type="text" class="form-control image-preview-filename"
                                 disabled="disabled"/>
                          <div class="input-group-btn">
                            <!-- image-preview-input -->
                            <div class="btn btn-default image-preview-input">
                              <span class="glyphicon glyphicon-folder-open"></span>
                              <span class="image-preview-input-title">Browse</span>
                              <input type="file" accept="image/png, image/jpeg" ref="file_chooser"
                                     name="poster-image" @change="previewImage"/>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-4 text-right" v-show="selected">
                        <a href="#" class="btn btn-info btn-outline-info" @click="cropImage">
                          <i class="fa fa-crop"></i> Crop Picture
                        </a>
                      </div>
                      <div class="row" v-show="cropped">
                        <hr/>
                        <div class="text-center " style="margin-top: 50px">
                          <a href="#" class="btn btn-success btn-lg" @click="downloadPoster()">
                            <i class="fa fa-save"></i> Download DP
                          </a>                          &nbsp; &nbsp;
      
                          <a href="#" class="btn btn-primary btn-lg" @click="shareOnFacebook()">
                            <i class="fa fa-facebook"></i> Share on Facebook
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6" v-show="cropped">
                <div class="row">
                  <div class="img-holder">
                    <div class="image-area" ref="image_area">
                      <img class="img-rounded img-thumbnail poster poster-uploaded" :src="upload.image_link"
                           v-show="selected"/>
                      <img class="img-rounded img-thumbnail poster poster-cover" :class="{'img-cover':selected}"
                           src="{{$poster_link}}"/>
                      <div class="name-label" :style="textStyle">
                        <span v-text="first_name" class="fnf"></span> <br/>
                        <span v-text="last_name" class="lnf"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  
  
  </div>
@endsection


@section('scripts')
  <script>
    window.fbAsyncInit = function () {
      FB.init({
        appId: '{{$facebook_app_id}}',
        autoLogAppEvents: true,
        xfbml: true,
        version: 'v2.12'
      });
    };

    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>
  <script src="{{url('js/make-poster.js')}}"></script>
@endsection
