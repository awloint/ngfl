@extends('layout.main')

@section('content')
<div id="root">
    <div class="row" style="margin-bottom:50px;">
        <div class="panel panel-info">
            <div class="panel-heading">
                Stats
            </div>
            <div class="panel-body">
                <div class="col-sm-6" style="text-align:center;">
                    <p style="font-size: 800%; color:blueviolet;">@{{nominees.length}}</p>
                    <span class='label label-info'>Nominees</span>
                </div>
                <div class="col-sm-6" style="text-align:center;">
                    <p style="font-size: 800%; color:blueviolet;">@{{nominator_count}}</p>
                    <span class='label label-info'>Nominators</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <table class="table table-striped table-bordered table-condensed">
        <thead>
            <tr class="bg-primary">
                <th>S/N</th>
                <th>Nominee</th>
                <th>City</th>
                <th>Nomination Count</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for='(nominee, x) in nominees'>
                <td>@{{x+1}}</td>
                <td>@{{fullName(nominee)}}</td>
                <td>@{{nominee.city}}</td>
                <td>@{{nominee.nomination_count}}</td>
            </tr>
        </tbody>
    </table>
    </div>
</div>
@endsection

@section('scripts')
<script src="/js/show-nominees.js"></script>
@endsection