<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" value="{{csrf_token()}}" />
  
  <title>NGFL Nomination Closed</title>
  
  <!-- Fonts -->
  <link href="https://" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
  
  <!-- Bootstrap -->
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  
  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
        integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  
  <!-- FlatUI Theme-->
  <link rel="stylesheet" href="css/flat-ui.min.css" type="text/css">
  
  <!-- Font Awesome -->
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  
  <style>
    body {
      padding: 40px 20px;
    }
    .badge-warning {
      background-color: #cc3838;
    }
  </style>
</head>
<body>
<div class="col-md-10 col-md-offset-1">
  <h1 style="line-height: 0px; margin-bottom:0px; margin-top: 50px; text-align:center;">#NGFL</h1> <br>
  <p style="margin-bottom: 30px; text-align:center;">NEXT GENERATION FEMALE LEADER</p>
    <p style="text-align:center;">
        <img src="src/nominations-closed.png">
    </p>
    <p style="text-align:center; padding-top:30px;">
        But you can click below to Make your DP
    </p>
    <p style="text-align:center;">
        <a href="/make-poster" class="btn btn-lg btn-success btn-wide btn-embossed">Create Your DP</a>
    </p>
</div>

<div class="col-md-10 col-md-offset-1">
  <p style="text-align:center; padding-top: 30px;">Copyright &copy; 2018 &nbsp; &nbsp; <a href="//awlo.org"
                                                                                          style="color:#f10f11;">AWLO</a>
    x <a href="//beautified.com.ng" style="color:#9200d1;">Beautified Network</a></p>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="js/flat-ui.min.js"></script>
<script src="js/app.js"></script>
</body>
</html>
