import Vue from "vue";
import axios from "axios";

new Vue({
  el: "#root",
  data: {
    nominees: [],
    x: 1
  },
  mounted() {
    this.loadNominees();
    setInterval(this.loadNominees, 5000);
  },
  methods: {
    loadNominees() {
      axios.get("/api/nominees").then(response => {
        this.x = 1;
        this.nominees = response.data;
      });
    },
    fullName(nominee) {
      return [nominee.first_name, nominee.middle_name, nominee.last_name].join(
        " "
      );
    }
  },
  computed: {
    nominator_count() {
        if( this.nominees.length == 0 ) {
            return 0;
        }

        return this.nominees.map(nominee => nominee.nomination_count).reduce((total, nomination_count) => {
            return total + nomination_count;
        });
    }
  }
});
