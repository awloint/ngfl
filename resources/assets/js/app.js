/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';
import swal from 'sweetalert2';
import axios from 'axios';
import DropDown from './components/DropDownSelect';
import NameWidget from './components/NameWidget';
import NameSearchField from './components/NameSearchField';

require('./bootstrap');

new Vue({
  el: '#root',
  components: {
    'dropdown': DropDown,
    'name-widget': NameWidget,
    'name-search': NameSearchField
  },
  data: {
    nominator: {},
    nominee: {},
    completed: false,
    allowed_cities: ['Lagos', 'Port Harcourt', 'Abuja'],
    busy: false
  },
  methods: {
    saveNomination() {
      var data = this.nominator;
      var nom = this.nominee;
      data.nominee = nom.name;
      data.reason = nom.reason;
      this.busy = true;

      axios.post('/api/nominees', data).then((response) => {
        //Send the sms notification..
        const notified = axios.post('/api/notify', {
          nominator: this.nominator
        });

        swal({
          type: 'success',
          title: 'Success.!!',
          text: response.data.msg
        }).then(() => {
          notified.then(() => {
            window.location = '/make-poster';
          });
        });
      }).catch((error) => {
        this.busy = false;
        swal({
          type: 'error',
          title: 'Oops..!.',
          text: error.response.data.msg
        });
      });
    },
    setValue({ name, value }) {
      this.nominator[name] = value;
    },
    setName(name, target) {
      if (target == 'nominator') {
        this.nominator = Object.assign(this.nominator, name);
      } else {
        this.nominee.name = name;
      }
    },
    checkCount() {
      if( this.rlExceeded ) {
        swal("Sorry.. you cannot exceed 200 characters", "Warning", "error").then(() => {
          this.nominee.reason = this.nominee.reason.substr(0, 200);
        });
      }
    }
  },
  computed: {
    rlExceeded() {
      return this.nominee.reason && this.nominee.reason.length > 200;
    },
    textCount() {
      return (this.nominee.reason ? this.nominee.reason.length : 0) + ' / 200';
    },
    rlMaxing() {
      return this.nominee.reason && this.nominee.reason.length >= 190;
    }
  }
});
