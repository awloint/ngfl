/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';
import { Croppie } from 'croppie';
import html2canvas from 'html2canvas';
import FileSaver from 'file-saver';
import axios from 'axios';

const $ = window.jQuery;

let cropper;

new Vue({
  el: '#root',
  data() {
    return {
      upload: {
        image_link: '#',
        status: 0,
        form_path: 'save-image', //make-poster, save-image
      },
      file: {
        name: ''
      },
      first_name: '',
      last_name: '',
      cropped: false,
      tposition: {
        left: 2,
        bottom: 24,
        font: 4,
        width: 39
      },
      image_dim: {}
    };
  },
  mounted() {
    $(() => {
      // and when done:
      setTimeout(() => {
        $('#content').waiting('done');
      }, 2000);
    });

    window.onresize = () => {
      this.setImageDim();
    };
  },
  computed: {
    selected() {
      return this.file.name != '';
    },
    textStyle() {
      //Set the text position..
      const left = (this.tposition.left / 100) * Math.floor(this.image_dim.width);
      const bottom = (this.tposition.bottom / 100) * Math.floor(this.image_dim.height);
      const font_size = (this.tposition.font / 100) * Math.floor(this.image_dim.width);
      const width = (this.tposition.width / 100) * Math.floor(this.image_dim.width);
      const text_style = {
        'margin-left': left + 'px',
        'margin-bottom': bottom + 'px',
        'font-size': font_size + 'px',
        'width': width + 'px'
      };

      return text_style;
    },
    image_dimension() {
      this.setImageDim();
      return this.getImageDim();
    }
  },
  methods: {
    previewImage() {
      const input = this.$refs['file_chooser'];
      const url = input.value;
      const ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

      this.file.name = url;

      if (input.files && input.files[0] && (ext == 'png' || ext == 'jpeg' || ext == 'jpg')) {
        const reader = new FileReader();

        reader.onload = (e) => {
          $(this.$refs['croppie-item']).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);

        setTimeout(this.initCroppie, 400);
      } else {
        $(this.$refs['croppie-item']).attr('src', '/assets/no_preview.png');
      }
    },
    initCroppie() {
      cropper = new Croppie(this.$refs['croppie-item'], {
        enableExif: true,
        viewport: {
          width: 400,
          height: 400,
          type: 'square',
          quality: 1,
          size: 'viewport'
        },
        boundary: {
          width: 450,
          height: 450
        }
      });
    },
    downloadPoster() {
      $('body').waiting();
      html2canvas(this.$refs['image_area']).then((canvas) => {
        canvas.toBlob((blob) => {
          FileSaver.saveAs(blob, 'poster.png');
          $('body').waiting('done');
        });
      });
    },
    cropImage() {
      //on button click
      cropper.result('canvas', 'original', 'png', 1).then((data_url) => {
        this.upload.image_link = data_url;
        this.cropped = true;

        setTimeout(() => {
          $('.img-holder').css({
            'height': $('img.poster-cover').height()
          });

          this.setImageDim();
        }, 100);
      });
    },
    getImageDim() {
      const width = $('.poster.poster-cover').width();
      const height = $('.poster.poster-cover').height();

      //Resize Image container
      const img_height = ($('img.poster.poster-cover').height() + 25);
      $('.image-area').height(img_height + 'px');
      $('.img-holder').height((img_height + 30) + 'px');

      const dim = {
        width: width,
        height: height
      };

      return dim;
    },
    setImageDim() {
      this.image_dim = this.getImageDim();
    },
    shareOnFacebook() {
      $('body').waiting();
      //Get the image data-url..
      html2canvas(this.$refs['image_area']).then((canvas) => {
        //Save it to the server..
        return axios.post('/api/upload-image', {
          'image_string': canvas.toDataURL(),
          'name': [this.first_name, this.last_name].join('_')
        })
            .then((response) => {
              $('body').waiting('done');
              this.initSharing(response.data.share_url);
            })
            .catch((res) => {
              console.log(res);
            });
      });
    },
    initSharing(url) {
      window.FB.ui({
        method: 'share',
        href: url,
      }, function (response) {

      });
    }
  }
});
