let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js');
mix.js('resources/assets/js/make-poster.js', 'public/js');
mix.js("resources/assets/js/show-nominees.js", "public/js");
   // .sass('resources/assets/sass/app.scss', 'public/css');

mix.copy('node_modules/croppie/croppie.css', 'public/css/croppie.css');
mix.copy('node_modules/font-awesome/css/font-awesome.css', 'public/css/font-awesome.css');

mix.copy('node_modules/jquery-waiting/dist/jquery.waiting.min.js', 'public/js/jquery.waiting.min.js');
mix.copy('node_modules/jquery-waiting/dist/waiting.css', 'public/css/waiting.css');

mix.copyDirectory('node_modules/font-awesome/fonts', 'public/fonts');
