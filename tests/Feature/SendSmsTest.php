<?php

namespace Tests\Feature;

use App\InfobipSMS;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SendSmsTest extends TestCase
{
   /**
    * A basic test example.
    *
    * @return void
    */
   public function test_sms_sending() {
      $message = "Hello Popsana..!!";
      $recipient = "+2348176424650";
      
      $this->assertTrue(true);
      
      return true;
      
      $sent = InfobipSMS::sendSMS($recipient, $message);
      
      $this->assertTrue(count($sent->getMessages()) > 0);
   }
   
   public function test_phone_number_formatting() {
      $this->assertTrue(true); return;
      $number = "08065589845";
      $formatted = InfobipSMS::formatPhone($number);
      
      $this->assertEquals("2348065589845", $formatted);
   }
}
