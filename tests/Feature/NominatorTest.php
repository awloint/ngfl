<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Nominator;
use App\Nominee;


class NominatorTest extends TestCase
{
   use RefreshDatabase;

   /**
    * A basic test example.
    *
    * @return void
    */
   public function test_add_new_nominator() {
      $data = factory(Nominator::class, 1)->make()->first()->getAttributes();
      $nominator = Nominator::add($data);

      $this->assertArrayHasKey("id", $nominator->toArray());
   }

   function test_nominator_can_nominate() {
      $nominator = factory(Nominator::class, 1)->create()->first();
      $nominee = factory(Nominee::class, 1)->create()->first();
      $nominator->nominate($nominee);

      $this->assertTrue($nominator->nominee != null);
   }

   function test_nominee_can_get_more_nominations() {
      $nominators = factory(Nominator::class, 20)->create();
      $nominee = factory(Nominee::class, 1)->create()->first();

      $nominators->each(function(Nominator $nom) use ($nominee) {
         $nom->nominate($nominee);
      });

      $this->assertTrue($nominee->nominationCount() == $nominators->count());
   
      $nominators2 = factory(Nominator::class, 20)->create();
      $nominee = Nominee::withInfo($nominee->full_name, $nominee->city);
   
      $nominators2->each(function(Nominator $nom) use ($nominee) {
         $nom->nominate($nominee);
      });
   
      
      $all_nominators_count = $nominators->count() + $nominators2->count();
      
      $this->assertTrue($nominee->nominationCount() == $all_nominators_count);
   }

   public function test_block_second_attempt() {
      $data = factory(Nominator::class, 1)->make()->first()->getAttributes();
      $this->expectException('Exception');

      Nominator::add($data);
      Nominator::add($data);
   }

   public function test_nomination_with_shuffled_nominee_name() {
      $nominators = factory(Nominator::class, 2)->create();
      $nominee = factory(Nominee::class, 1)->make()->first();
      $nominators[0]->nominate( $nominee );
      $shuffled_name = implode(" ", [$nominee->last_name, $nominee->first_name, $nominee->middle_name]);
      $nominee = Nominee::lookFor($shuffled_name)->first();
      $nominators[1]->nominate( $nominee );

      $this->assertTrue($nominee->nomination_count == 2, "didn't match nominee when name was shuffled");
   }
}
