<?php

namespace Tests\Feature;

use App\Nominee;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NomineeTest extends TestCase
{
   use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_add_nominee_from_full_name()
    {
       $nominee = Nominee::makeFromName("Popsana Barida");

       $this->assertArrayHasKey("first_name", $nominee);
       $this->assertArrayHasKey("last_name", $nominee);
       $this->assertTrue($nominee->middle_name == "");
    }

   public function test_add_nominee_splits_names()
   {
      $nominee = Nominee::makeFromName("Popsana Barida Noble");

      $this->assertTrue($nominee->first_name == "Popsana");
      $this->assertTrue($nominee->last_name == "Barida");
      $this->assertTrue($nominee->middle_name == "Noble");
   }

   public function test_add_nominee_parses_names()
   {
      $nominee = Nominee::makeFromName("Popsana Barida Noble");

      $this->assertTrue($nominee->first_name == "Popsana");
      $this->assertTrue($nominee->last_name == "Barida");
      $this->assertTrue($nominee->middle_name == "Noble");

      $nominee1 = Nominee::makeFromName("Popsana Barida");

      $this->assertTrue($nominee1->first_name == "Popsana");
      $this->assertTrue($nominee1->last_name == "Barida");


      $nominee = Nominee::makeFromName("Popsana  Barida Noble");

      $this->assertTrue($nominee->first_name == "Popsana");
      $this->assertTrue($nominee->last_name == "Barida");
      $this->assertTrue($nominee->middle_name == "Noble");

      $nominee1 = Nominee::makeFromName("Popsana  Barida");

      $this->assertTrue($nominee1->first_name == "Popsana");
      $this->assertTrue($nominee1->last_name == "Barida");
   }
   
   function test_nominee_autocomplete_search() {
      $nominees = factory(Nominee::class, 4)->create();
      
      foreach($nominees as $nominee) {
         $this->assertGreaterThanOrEqual(1, Nominee::lookFor($nominee->first_name)->count());
         $this->assertGreaterThanOrEqual(1, Nominee::lookFor($nominee->last_name)->count());
         $this->assertGreaterThanOrEqual(1, Nominee::lookFor("{$nominee->first_name} {$nominee->last_name}")->count());
         $this->assertGreaterThanOrEqual(1, Nominee::lookFor("{$nominee->first_name} {$nominee->last_name} {$nominee->middle_name}")->count());
         $this->assertGreaterThanOrEqual(1, Nominee::lookFor("{$nominee->first_name} {$nominee->middle_name} {$nominee->last_name}")->count());
         $this->assertGreaterThanOrEqual(1, Nominee::lookFor($nominee->name)->count());
      }
   }
}
